﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Text.Unicode;
using System.Text.RegularExpressions;

namespace Кузьмина_Аленв_Lab8_ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\LENOVO\Desktop\программирование\lab8\Text.txt";
            string vpath = @"..\..\..\..\..\..\Text.txt";
            FileInfo info = new FileInfo(path);
            string Text = File.ReadAllText(path);
            string[] textMass;
            textMass = Text.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            var result = "result.txt";

            using (StreamWriter streamWriter = new StreamWriter(result))
            {
                streamWriter.WriteLine("Название файла: " + info.Name);
                streamWriter.WriteLine("Абсолютный путь: " + info.FullName);
                streamWriter.WriteLine("Относительный путь: " + vpath);
                streamWriter.WriteLine("Время создания: " + info.CreationTime);
                streamWriter.WriteLine("Размер файла: " + info.Length);

                streamWriter.WriteLine("Количество слов: " + textMass.Length);
                streamWriter.WriteLine("Количество строк: " + File.ReadAllLines(path).Length);
                streamWriter.WriteLine("Присутствуют ли в тексте цифры?: " + Regex.IsMatch(Text, @"[0-9]"));
                streamWriter.WriteLine("Есть кириллица?: " + Regex.IsMatch(Text, @"[A-z]"));
                streamWriter.WriteLine("Есть латиница?: " + Regex.IsMatch(Text, @"[A-z]"));

            }
        }
    }
}

﻿using System;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.IO;



namespace Кузьмина_Алена_lab8_ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"C:\Users\LENOVO\Desktop\программирование\lab8\Table.csv";
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(1251);

            var lines = File.ReadAllLines(path);
            var persons = new Person[lines.Length - 1];

            for (var i = 1; i < lines.Length; i++)
            {
                var splits = lines[i].Split(';');
                var person = new Person();
                person.Фамилия = splits[0];
                person.Время_работы = Convert.ToDouble(splits[1]);
                person.Зарплата = Convert.ToDouble(splits[2]);
                person.Чаевые = Convert.ToDouble(splits[3]);
                persons[i - 1] = person;
                Console.WriteLine(person);
            }
            var result = "Result.csv";
            using (StreamWriter streamWriter = new StreamWriter(result, false, encoding))
            {
                streamWriter.WriteLine($"Фамилия;Время_работы;Зарплата;Чаевые;Общая_прибыль;Деньги_в_час");
                for (int i = 0; i < persons.Length; i++)
                {
                    streamWriter.WriteLine(persons[i].ToExcel());
                }
            }
            var jsonOptions = new JsonSerializerOptions()
            {
                Encoder = JavaScriptEncoder.Default
            };

            var json = JsonSerializer.Serialize(persons, jsonOptions);
            File.WriteAllText("result.json", json);
        }
        public class Person
        {
            public string Фамилия { get; set; }
            public double Время_работы { get; set; }
            public double Зарплата { get; set; }
            public double Чаевые { get; set; }
            public double Общая_прибыль { get => Зарплата + Чаевые; }
            public double Деньги_в_час { get => Зарплата / Время_работы; }
            public override string ToString()
            {
                return $"{Фамилия} {Время_работы} {Зарплата} {Чаевые} {Общая_прибыль} {Деньги_в_час}";
            }

            public string ToExcel()
            {
                return $"{Фамилия};{Время_работы};{Зарплата};{Чаевые};{Общая_прибыль};{Деньги_в_час}";
            }
        }
    }
}